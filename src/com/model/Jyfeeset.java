package com.model;

/**
 * Jyfeeset entity. @author MyEclipse Persistence Tools
 */

public class Jyfeeset implements java.io.Serializable {

	// Fields

	private Integer id;
	private String fee;

	// Constructors

	/** default constructor */
	public Jyfeeset() {
	}

	/** full constructor */
	public Jyfeeset(String fee) {
		this.fee = fee;
	}

	// Property accessors

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getFee() {
		return this.fee;
	}

	public void setFee(String fee) {
		this.fee = fee;
	}

}