package com.model;

/**
 * Answer entity. @author MyEclipse Persistence Tools
 */

public class Answer implements java.io.Serializable {

	// Fields

	private Integer id;
	private String qstid;
	private String opid;
	private String mid;

	// Constructors

	/** default constructor */
	public Answer() {
	}

	/** full constructor */
	public Answer(String qstid, String opid, String mid) {
		this.qstid = qstid;
		this.opid = opid;
		this.mid = mid;
	}

	// Property accessors

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getQstid() {
		return this.qstid;
	}

	public void setQstid(String qstid) {
		this.qstid = qstid;
	}

	public String getOpid() {
		return this.opid;
	}

	public void setOpid(String opid) {
		this.opid = opid;
	}

	public String getMid() {
		return this.mid;
	}

	public void setMid(String mid) {
		this.mid = mid;
	}

}