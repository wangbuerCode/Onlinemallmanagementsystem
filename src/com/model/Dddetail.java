package com.model;

/**
 * Dddetail entity. @author MyEclipse Persistence Tools
 */

public class Dddetail implements java.io.Serializable {

	// Fields

	private Integer id;
	private String ddno;
	private String goodsid;
	private String num;
	private String memberid;
	private String fkstatus;

	// Constructors

	/** default constructor */
	public Dddetail() {
	}

	/** full constructor */
	public Dddetail(String ddno, String goodsid, String num, String memberid,
			String fkstatus) {
		this.ddno = ddno;
		this.goodsid = goodsid;
		this.num = num;
		this.memberid = memberid;
		this.fkstatus = fkstatus;
	}

	// Property accessors

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getDdno() {
		return this.ddno;
	}

	public void setDdno(String ddno) {
		this.ddno = ddno;
	}

	public String getGoodsid() {
		return this.goodsid;
	}

	public void setGoodsid(String goodsid) {
		this.goodsid = goodsid;
	}

	public String getNum() {
		return this.num;
	}

	public void setNum(String num) {
		this.num = num;
	}

	public String getMemberid() {
		return this.memberid;
	}

	public void setMemberid(String memberid) {
		this.memberid = memberid;
	}

	public String getFkstatus() {
		return this.fkstatus;
	}

	public void setFkstatus(String fkstatus) {
		this.fkstatus = fkstatus;
	}

}