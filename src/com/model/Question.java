package com.model;

/**
 * Question entity. @author MyEclipse Persistence Tools
 */

public class Question implements java.io.Serializable {

	// Fields

	private Integer id;
	private String tm;
	private String op1;
	private String op2;
	private String op3;
	private String op4;

	// Constructors

	/** default constructor */
	public Question() {
	}

	/** full constructor */
	public Question(String tm, String op1, String op2, String op3, String op4) {
		this.tm = tm;
		this.op1 = op1;
		this.op2 = op2;
		this.op3 = op3;
		this.op4 = op4;
	}

	// Property accessors

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getTm() {
		return this.tm;
	}

	public void setTm(String tm) {
		this.tm = tm;
	}

	public String getOp1() {
		return this.op1;
	}

	public void setOp1(String op1) {
		this.op1 = op1;
	}

	public String getOp2() {
		return this.op2;
	}

	public void setOp2(String op2) {
		this.op2 = op2;
	}

	public String getOp3() {
		return this.op3;
	}

	public void setOp3(String op3) {
		this.op3 = op3;
	}

	public String getOp4() {
		return this.op4;
	}

	public void setOp4(String op4) {
		this.op4 = op4;
	}

}