package com.model;

/**
 * Member entity. @author MyEclipse Persistence Tools
 */

public class Member implements java.io.Serializable {

	// Fields

	private Integer id;
	private String username;
	private String userpassword;
	private String realname;
	private String sex;
	private String tel;
	private String brithtime;
	private String addr;
	private String email;
	private String savetime;
	private String delstatus;
	private String filename;
	private Integer loginerronum;
	private String lockstatus;
	private String isgjhy;
	private String sheng;
	private String shi;

	// Constructors

	/** default constructor */
	public Member() {
	}

	/** full constructor */
	public Member(String username, String userpassword, String realname,
			String sex, String tel, String brithtime, String addr,
			String email, String savetime, String delstatus, String filename,
			Integer loginerronum, String lockstatus, String isgjhy,
			String sheng, String shi) {
		this.username = username;
		this.userpassword = userpassword;
		this.realname = realname;
		this.sex = sex;
		this.tel = tel;
		this.brithtime = brithtime;
		this.addr = addr;
		this.email = email;
		this.savetime = savetime;
		this.delstatus = delstatus;
		this.filename = filename;
		this.loginerronum = loginerronum;
		this.lockstatus = lockstatus;
		this.isgjhy = isgjhy;
		this.sheng = sheng;
		this.shi = shi;
	}

	// Property accessors

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getUsername() {
		return this.username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getUserpassword() {
		return this.userpassword;
	}

	public void setUserpassword(String userpassword) {
		this.userpassword = userpassword;
	}

	public String getRealname() {
		return this.realname;
	}

	public void setRealname(String realname) {
		this.realname = realname;
	}

	public String getSex() {
		return this.sex;
	}

	public void setSex(String sex) {
		this.sex = sex;
	}

	public String getTel() {
		return this.tel;
	}

	public void setTel(String tel) {
		this.tel = tel;
	}

	public String getBrithtime() {
		return this.brithtime;
	}

	public void setBrithtime(String brithtime) {
		this.brithtime = brithtime;
	}

	public String getAddr() {
		return this.addr;
	}

	public void setAddr(String addr) {
		this.addr = addr;
	}

	public String getEmail() {
		return this.email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getSavetime() {
		return this.savetime;
	}

	public void setSavetime(String savetime) {
		this.savetime = savetime;
	}

	public String getDelstatus() {
		return this.delstatus;
	}

	public void setDelstatus(String delstatus) {
		this.delstatus = delstatus;
	}

	public String getFilename() {
		return this.filename;
	}

	public void setFilename(String filename) {
		this.filename = filename;
	}

	public Integer getLoginerronum() {
		return this.loginerronum;
	}

	public void setLoginerronum(Integer loginerronum) {
		this.loginerronum = loginerronum;
	}

	public String getLockstatus() {
		return this.lockstatus;
	}

	public void setLockstatus(String lockstatus) {
		this.lockstatus = lockstatus;
	}

	public String getIsgjhy() {
		return this.isgjhy;
	}

	public void setIsgjhy(String isgjhy) {
		this.isgjhy = isgjhy;
	}

	public String getSheng() {
		return this.sheng;
	}

	public void setSheng(String sheng) {
		this.sheng = sheng;
	}

	public String getShi() {
		return this.shi;
	}

	public void setShi(String shi) {
		this.shi = shi;
	}

}