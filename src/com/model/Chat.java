package com.model;

/**
 * Chat entity. @author MyEclipse Persistence Tools
 */

public class Chat implements java.io.Serializable {

	// Fields

	private Integer id;
	private String memberid;
	private String content;
	private String hfcontent;
	private String savetime;
	private String hfsavetime;

	// Constructors

	/** default constructor */
	public Chat() {
	}

	/** full constructor */
	public Chat(String memberid, String content, String hfcontent,
			String savetime, String hfsavetime) {
		this.memberid = memberid;
		this.content = content;
		this.hfcontent = hfcontent;
		this.savetime = savetime;
		this.hfsavetime = hfsavetime;
	}

	// Property accessors

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getMemberid() {
		return this.memberid;
	}

	public void setMemberid(String memberid) {
		this.memberid = memberid;
	}

	public String getContent() {
		return this.content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public String getHfcontent() {
		return this.hfcontent;
	}

	public void setHfcontent(String hfcontent) {
		this.hfcontent = hfcontent;
	}

	public String getSavetime() {
		return this.savetime;
	}

	public void setSavetime(String savetime) {
		this.savetime = savetime;
	}

	public String getHfsavetime() {
		return this.hfsavetime;
	}

	public void setHfsavetime(String hfsavetime) {
		this.hfsavetime = hfsavetime;
	}

}