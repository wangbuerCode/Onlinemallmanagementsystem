package com.model;

/**
 * Loginfo entity. @author MyEclipse Persistence Tools
 */

public class Loginfo implements java.io.Serializable {

	// Fields

	private Integer id;
	private Integer uid;
	private String info;
	private String savetime;

	// Constructors

	/** default constructor */
	public Loginfo() {
	}

	/** full constructor */
	public Loginfo(Integer uid, String info, String savetime) {
		this.uid = uid;
		this.info = info;
		this.savetime = savetime;
	}

	// Property accessors

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getUid() {
		return this.uid;
	}

	public void setUid(Integer uid) {
		this.uid = uid;
	}

	public String getInfo() {
		return this.info;
	}

	public void setInfo(String info) {
		this.info = info;
	}

	public String getSavetime() {
		return this.savetime;
	}

	public void setSavetime(String savetime) {
		this.savetime = savetime;
	}

}