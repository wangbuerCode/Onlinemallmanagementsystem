package com.model;

/**
 * Wlinfo entity. @author MyEclipse Persistence Tools
 */

public class Wlinfo implements java.io.Serializable {

	// Fields

	private Integer id;
	private String ddid;
	private String info;
	private String savetime;

	// Constructors

	/** default constructor */
	public Wlinfo() {
	}

	/** full constructor */
	public Wlinfo(String ddid, String info, String savetime) {
		this.ddid = ddid;
		this.info = info;
		this.savetime = savetime;
	}

	// Property accessors

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getDdid() {
		return this.ddid;
	}

	public void setDdid(String ddid) {
		this.ddid = ddid;
	}

	public String getInfo() {
		return this.info;
	}

	public void setInfo(String info) {
		this.info = info;
	}

	public String getSavetime() {
		return this.savetime;
	}

	public void setSavetime(String savetime) {
		this.savetime = savetime;
	}

}