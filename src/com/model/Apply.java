package com.model;

/**
 * Apply entity. @author MyEclipse Persistence Tools
 */

public class Apply implements java.io.Serializable {

	// Fields

	private Integer id;
	private String mid;
	private String shstatus;

	// Constructors

	/** default constructor */
	public Apply() {
	}

	/** full constructor */
	public Apply(String mid, String shstatus) {
		this.mid = mid;
		this.shstatus = shstatus;
	}

	// Property accessors

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getMid() {
		return this.mid;
	}

	public void setMid(String mid) {
		this.mid = mid;
	}

	public String getShstatus() {
		return this.shstatus;
	}

	public void setShstatus(String shstatus) {
		this.shstatus = shstatus;
	}

}