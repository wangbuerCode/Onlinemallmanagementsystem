package com.model;

/**
 * Goods entity. @author MyEclipse Persistence Tools
 */

public class Goods implements java.io.Serializable {

	// Fields

	private Integer id;
	private String goodno;
	private String goodname;
	private String fid;
	private String sid;
	private String goodpp;
	private String price;
	private String tprice;
	private String istj;
	private String delstatus;
	private String savetime;
	private String filename1;
	private String filename2;
	private String filename3;
	private String filename4;
	private String content;
	private Integer num;

	// Constructors

	/** default constructor */
	public Goods() {
	}

	/** full constructor */
	public Goods(String goodno, String goodname, String fid, String sid,
			String goodpp, String price, String tprice, String istj,
			String delstatus, String savetime, String filename1,
			String filename2, String filename3, String filename4, String content) {
		this.goodno = goodno;
		this.goodname = goodname;
		this.fid = fid;
		this.sid = sid;
		this.goodpp = goodpp;
		this.price = price;
		this.tprice = tprice;
		this.istj = istj;
		this.delstatus = delstatus;
		this.savetime = savetime;
		this.filename1 = filename1;
		this.filename2 = filename2;
		this.filename3 = filename3;
		this.filename4 = filename4;
		this.content = content;
	}

	// Property accessors

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getGoodno() {
		return this.goodno;
	}

	public void setGoodno(String goodno) {
		this.goodno = goodno;
	}

	public String getGoodname() {
		return this.goodname;
	}

	public void setGoodname(String goodname) {
		this.goodname = goodname;
	}

	public String getFid() {
		return this.fid;
	}

	public void setFid(String fid) {
		this.fid = fid;
	}

	public String getSid() {
		return this.sid;
	}

	public void setSid(String sid) {
		this.sid = sid;
	}

	public String getGoodpp() {
		return this.goodpp;
	}

	public void setGoodpp(String goodpp) {
		this.goodpp = goodpp;
	}

	public String getPrice() {
		return this.price;
	}

	public void setPrice(String price) {
		this.price = price;
	}

	public String getTprice() {
		return this.tprice;
	}

	public void setTprice(String tprice) {
		this.tprice = tprice;
	}

	public String getIstj() {
		return this.istj;
	}

	public void setIstj(String istj) {
		this.istj = istj;
	}

	public String getDelstatus() {
		return this.delstatus;
	}

	public void setDelstatus(String delstatus) {
		this.delstatus = delstatus;
	}

	public String getSavetime() {
		return this.savetime;
	}

	public void setSavetime(String savetime) {
		this.savetime = savetime;
	}

	public String getFilename1() {
		return this.filename1;
	}

	public void setFilename1(String filename1) {
		this.filename1 = filename1;
	}

	public String getFilename2() {
		return this.filename2;
	}

	public void setFilename2(String filename2) {
		this.filename2 = filename2;
	}

	public String getFilename3() {
		return this.filename3;
	}

	public void setFilename3(String filename3) {
		this.filename3 = filename3;
	}

	public String getFilename4() {
		return this.filename4;
	}

	public void setFilename4(String filename4) {
		this.filename4 = filename4;
	}

	public String getContent() {
		return this.content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public Integer getNum() {
		return num;
	}

	public void setNum(Integer num) {
		this.num = num;
	}

	

}