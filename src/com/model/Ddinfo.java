package com.model;

/**
 * Ddinfo entity. @author MyEclipse Persistence Tools
 */

public class Ddinfo implements java.io.Serializable {

	// Fields

	private Integer id;
	private String ddno;
	private String memberid;
	private String ddtotal;
	private String fkstatus;
	private String shstatus;
	private String savetime;
	private String lxfs;
	private String fhstatus;
	private String wlinfo;
	private String remark;
	private String days;

	// Constructors

	/** default constructor */
	public Ddinfo() {
	}

	/** full constructor */
	public Ddinfo(String ddno, String memberid, String ddtotal,
			String fkstatus, String shstatus, String savetime, String lxfs,
			String fhstatus, String wlinfo, String remark) {
		this.ddno = ddno;
		this.memberid = memberid;
		this.ddtotal = ddtotal;
		this.fkstatus = fkstatus;
		this.shstatus = shstatus;
		this.savetime = savetime;
		this.lxfs = lxfs;
		this.fhstatus = fhstatus;
		this.wlinfo = wlinfo;
		this.remark = remark;
	}

	// Property accessors

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getDdno() {
		return this.ddno;
	}

	public void setDdno(String ddno) {
		this.ddno = ddno;
	}

	public String getMemberid() {
		return this.memberid;
	}

	public void setMemberid(String memberid) {
		this.memberid = memberid;
	}

	public String getDdtotal() {
		return this.ddtotal;
	}

	public void setDdtotal(String ddtotal) {
		this.ddtotal = ddtotal;
	}

	public String getFkstatus() {
		return this.fkstatus;
	}

	public void setFkstatus(String fkstatus) {
		this.fkstatus = fkstatus;
	}

	public String getShstatus() {
		return this.shstatus;
	}

	public void setShstatus(String shstatus) {
		this.shstatus = shstatus;
	}

	public String getSavetime() {
		return this.savetime;
	}

	public void setSavetime(String savetime) {
		this.savetime = savetime;
	}

	public String getLxfs() {
		return this.lxfs;
	}

	public void setLxfs(String lxfs) {
		this.lxfs = lxfs;
	}

	public String getFhstatus() {
		return this.fhstatus;
	}

	public void setFhstatus(String fhstatus) {
		this.fhstatus = fhstatus;
	}

	public String getWlinfo() {
		return this.wlinfo;
	}

	public void setWlinfo(String wlinfo) {
		this.wlinfo = wlinfo;
	}

	public String getRemark() {
		return this.remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public String getDays() {
		return days;
	}

	public void setDays(String days) {
		this.days = days;
	}

}