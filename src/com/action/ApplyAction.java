package com.action;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts2.ServletActionContext;

import com.dao.CommonDAO;
import com.model.Apply;
import com.model.Chat;
import com.model.Dddetail;
import com.model.Goods;
import com.model.Imgadv;
import com.model.Member;
import com.model.News;
import com.model.Pingjia;
import com.model.Protype;
import com.model.Sysuser;
import com.opensymphony.xwork2.ActionSupport;
import com.util.Info;
import com.util.MD5;
import com.util.Pagination;

/**
 * 高级会员申请功能
 * @author Administrator
 *
 */
public class ApplyAction extends ActionSupport
{
	private Integer id;
	private String mid;
	private String shstatus;
	String suc;
	String error;
	String no;
	private int index=1;
	private CommonDAO commonDAO;
	
	
	

	public Integer getId() {
		return id;
	}


	public void setId(Integer id) {
		this.id = id;
	}


	public String getMid() {
		return mid;
	}


	public void setMid(String mid) {
		this.mid = mid;
	}


	public String getShstatus() {
		return shstatus;
	}


	public void setShstatus(String shstatus) {
		this.shstatus = shstatus;
	}


	public String getSuc() {
		return suc;
	}


	public void setSuc(String suc) {
		this.suc = suc;
	}


	public String getError() {
		return error;
	}


	public void setError(String error) {
		this.error = error;
	}


	public String getNo() {
		return no;
	}


	public void setNo(String no) {
		this.no = no;
	}


	public int getIndex() {
		return index;
	}


	public void setIndex(int index) {
		this.index = index;
	}


	public CommonDAO getCommonDAO() {
		return commonDAO;
	}


	public void setCommonDAO(CommonDAO commonDAO) {
		this.commonDAO = commonDAO;
	}


	//新增系统用户前检查用户名唯一性 jqery
	 public void applyadd() throws IOException
	 {
		 Apply a = new Apply();
			Map session= ServletActionContext.getContext().getSession();
			Member member = (Member)session.get("member"); 
			ArrayList<Apply> list = (ArrayList<Apply>)commonDAO.findByHql("from Apply where mid='"+member.getId()+"' and shstatus='待审核'");
			a.setMid(member.getId().toString());
			a.setShstatus("待审核");
			if(list.size()==0){
				commonDAO.save(a);
			}
			
	    	 int responseContext;
	    		 responseContext = 0;
	 		HttpServletResponse response=ServletActionContext.getResponse();
	 		response.setContentType("text/html");
	 		PrintWriter out = response.getWriter();
	 		out.println(responseContext);
	 		out.flush();
	 		out.close();
	 }
		public String applyty(){
			Apply a = (Apply)commonDAO.findById(id, "Apply");
			a.setShstatus("同意");
			commonDAO.update(a);
			Member m = (Member)commonDAO.findById(a.getMid(), "Member");
			m.setIsgjhy("y");
			commonDAO.update(m);
			Map session= ServletActionContext.getContext().getSession();
			Sysuser s = (Sysuser)session.get("admin");
			commonDAO.writeLog(s.getId(), "同意申请");
			return "success";
		}
	public String applyjj(){
		Apply a = (Apply)commonDAO.findById(id, "Apply");
		a.setShstatus("拒绝");
		commonDAO.update(a);
		Map session= ServletActionContext.getContext().getSession();
		Sysuser s = (Sysuser)session.get("admin");
		commonDAO.writeLog(s.getId(), "拒绝申请");
		return "success";
	}
	
}
