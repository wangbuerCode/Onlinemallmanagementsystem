package com.action;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts2.ServletActionContext;

import com.dao.CommonDAO;
import com.model.Answer;
import com.model.Dddetail;
import com.model.Goods;
import com.model.Imgadv;
import com.model.Member;
import com.model.News;
import com.model.Pingjia;
import com.model.Protype;
import com.model.Sysuser;
import com.opensymphony.xwork2.ActionSupport;
import com.util.Info;
import com.util.MD5;
import com.util.Pagination;

/**
 * 问卷回答功能
 * @author Administrator
 *
 */
public class AnswerAction extends ActionSupport
{
	private Integer id;
	private String qstid;
	private String opid;
	private String mid;
	String suc;
	String error;
	String no;
	private int index=1;
	private CommonDAO commonDAO;
	
	
	
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getQstid() {
		return qstid;
	}

	public void setQstid(String qstid) {
		this.qstid = qstid;
	}

	public String getOpid() {
		return opid;
	}

	public void setOpid(String opid) {
		this.opid = opid;
	}

	public String getMid() {
		return mid;
	}

	public void setMid(String mid) {
		this.mid = mid;
	}

	public String getSuc() {
		return suc;
	}

	public void setSuc(String suc) {
		this.suc = suc;
	}

	public String getError() {
		return error;
	}

	public void setError(String error) {
		this.error = error;
	}

	public String getNo() {
		return no;
	}

	public void setNo(String no) {
		this.no = no;
	}

	public int getIndex() {
		return index;
	}

	public void setIndex(int index) {
		this.index = index;
	}

	public CommonDAO getCommonDAO() {
		return commonDAO;
	}

	public void setCommonDAO(CommonDAO commonDAO) {
		this.commonDAO = commonDAO;
	}

	//新增
	public String asAdd(){
		HttpServletRequest request = ServletActionContext.getRequest();
		Member member = (Member)request.getSession().getAttribute("member");
		if(member!=null){
			Answer as = new Answer();
			as.setMid(member.getId().toString());
			as.setOpid(opid);
			as.setQstid(qstid);
			commonDAO.save(as);
		}else{
			no="请先登录";
		}
		return "success";
	}
	
}
