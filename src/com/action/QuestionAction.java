package com.action;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts2.ServletActionContext;

import com.dao.CommonDAO;
import com.model.Goods;
import com.model.Imgadv;
import com.model.Member;
import com.model.News;
import com.model.Protype;
import com.model.Question;
import com.model.Sysuser;
import com.opensymphony.xwork2.ActionSupport;
import com.util.Info;
import com.util.MD5;
import com.util.Pagination;

/**
 * 问卷功能
 * @author Administrator
 *
 */
public class QuestionAction extends ActionSupport
{
	private Integer id;
	private String tm;
	private String op1;
	private String op2;
	private String op3;
	private String op4;
	String suc;
	String no;
	private int index=1;
	private CommonDAO commonDAO;
	
	
	//新增问卷
	public String qstAdd(){
		Question q = new Question();
		q.setTm(tm);
		q.setOp1(op1);
		q.setOp2(op2);
		q.setOp3(op3);
		q.setOp4(op4);
		commonDAO.save(q);
		Map session= ServletActionContext.getContext().getSession();
		Sysuser s = (Sysuser)session.get("admin");
		commonDAO.writeLog(s.getId(), "新增问卷");
		suc="";
		return "success";
	}
	//编辑
	public String qstEdit(){
		Question q = (Question)commonDAO.findById(id, "Question");
		q.setTm(tm);
		q.setOp1(op1);
		q.setOp2(op2);
		q.setOp3(op3);
		q.setOp4(op4);
		commonDAO.update(q);
		Map session= ServletActionContext.getContext().getSession();
		Sysuser s = (Sysuser)session.get("admin");
		commonDAO.writeLog(s.getId(), "编辑问卷");
		suc="";
		return "success";
	}
	
	//删除
	public String qstDel(){
		Question q = (Question)commonDAO.findById(id, "Question");
		commonDAO.delete(q);
		Map session= ServletActionContext.getContext().getSession();
		Sysuser s = (Sysuser)session.get("admin");
		commonDAO.writeLog(s.getId(), "删除问卷");
		suc="";
		return "success";
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getTm() {
		return tm;
	}
	public void setTm(String tm) {
		this.tm = tm;
	}
	public String getOp1() {
		return op1;
	}
	public void setOp1(String op1) {
		this.op1 = op1;
	}
	public String getOp2() {
		return op2;
	}
	public void setOp2(String op2) {
		this.op2 = op2;
	}
	public String getOp3() {
		return op3;
	}
	public void setOp3(String op3) {
		this.op3 = op3;
	}
	public String getOp4() {
		return op4;
	}
	public void setOp4(String op4) {
		this.op4 = op4;
	}
	public String getSuc() {
		return suc;
	}
	public void setSuc(String suc) {
		this.suc = suc;
	}
	public String getNo() {
		return no;
	}
	public void setNo(String no) {
		this.no = no;
	}
	public int getIndex() {
		return index;
	}
	public void setIndex(int index) {
		this.index = index;
	}
	public CommonDAO getCommonDAO() {
		return commonDAO;
	}
	public void setCommonDAO(CommonDAO commonDAO) {
		this.commonDAO = commonDAO;
	}
	
	
	
}
