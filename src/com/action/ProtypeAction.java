package com.action;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts2.ServletActionContext;

import com.dao.CommonDAO;
import com.google.gson.Gson;
import com.model.Imgadv;
import com.model.Member;
import com.model.Protype;
import com.model.Sysuser;
import com.opensymphony.xwork2.ActionSupport;
import com.util.Info;
import com.util.MD5;
import com.util.Pagination;

/**
 * 商品类别 功能
 * @author Administrator
 *
 */
public class ProtypeAction extends ActionSupport
{
	private Integer id;
	private String typename;
	private String fatherid;
	String suc;
	String no;
	private int index=1;
	private CommonDAO commonDAO;
	private File upFile;
	private String upFileContentType;
	private String upFileFileName;
	private static final int FILE_SIZE=16*1024;
	
	
	
	
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getTypename() {
		return typename;
	}

	public void setTypename(String typename) {
		this.typename = typename;
	}

	public String getFatherid() {
		return fatherid;
	}

	public void setFatherid(String fatherid) {
		this.fatherid = fatherid;
	}

	public String getSuc() {
		return suc;
	}

	public void setSuc(String suc) {
		this.suc = suc;
	}

	public String getNo() {
		return no;
	}

	public void setNo(String no) {
		this.no = no;
	}

	public int getIndex() {
		return index;
	}

	public void setIndex(int index) {
		this.index = index;
	}

	public CommonDAO getCommonDAO() {
		return commonDAO;
	}

	public void setCommonDAO(CommonDAO commonDAO) {
		this.commonDAO = commonDAO;
	}

	public File getUpFile() {
		return upFile;
	}

	public void setUpFile(File upFile) {
		this.upFile = upFile;
	}

	public String getUpFileContentType() {
		return upFileContentType;
	}

	public void setUpFileContentType(String upFileContentType) {
		this.upFileContentType = upFileContentType;
	}

	public String getUpFileFileName() {
		return upFileFileName;
	}

	public void setUpFileFileName(String upFileFileName) {
		this.upFileFileName = upFileFileName;
	}

	public static int getFileSize() {
		return FILE_SIZE;
	}

	
	//新增大类
	public String fprotypeAdd(){
		Protype protype = new Protype();
		if(fatherid.equals("0")){
		protype.setTypename(typename);
		protype.setFatherid("0");
		protype.setDelstatus("0");
		}else{
		protype.setTypename(typename);
		protype.setFatherid(fatherid);
		protype.setDelstatus("0");	
		}
		commonDAO.save(protype);
		Map session= ServletActionContext.getContext().getSession();
		Sysuser s = (Sysuser)session.get("admin");
		commonDAO.writeLog(s.getId(), "新增分类");
		suc="";
		return "success";
	}
	//编辑大类
	public String fprotypeEdit(){
		Protype protype = (Protype)commonDAO.findById(id, "Protype");
		protype.setTypename(typename);
		commonDAO.update(protype);
		Map session= ServletActionContext.getContext().getSession();
		Sysuser s = (Sysuser)session.get("admin");
		commonDAO.writeLog(s.getId(), "编辑分类");
		suc="";
		return "success";
	}
	//二级联动
	public void searchsontype() throws IOException{
		ArrayList<Protype> list = (ArrayList<Protype>)commonDAO.findByHql("from Protype where delstatus='0' and fatherid='"+fatherid+"'");
		String optstring="";
		if(!fatherid.equals("")){
		for(Protype protype:list){
			optstring+="<option value="+protype.getId()+">"+protype.getTypename()+"</option>";
		}
		}else{
			optstring="<option value=\"\">"+"请选择小类</option>";
		}
		HttpServletResponse response=ServletActionContext.getResponse();
		response.setCharacterEncoding("utf-8");
		response.setContentType("application/json");
		Gson gson = new Gson();
		String flag = gson.toJson(optstring);
		PrintWriter out = response.getWriter();
		out.write(flag);;
	}
	
	
}
