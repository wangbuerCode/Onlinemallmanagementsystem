package com.action;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts2.ServletActionContext;

import com.dao.CommonDAO;
import com.google.gson.Gson;
import com.model.Address;
import com.model.Car;
import com.model.Collect;
import com.model.Imgadv;
import com.model.Member;
import com.model.Protype;
import com.model.Sysuser;
import com.opensymphony.xwork2.ActionSupport;
import com.util.Info;
import com.util.MD5;
import com.util.Pagination;

/**
 * 会员收藏 功能
 * 
 * @author Administrator
 * 
 */
public class CollectAction extends ActionSupport {
	private Integer id;
	private String memberid;
	private String goodsid;
	String suc;
	String no;
	private int index = 1;
	private CommonDAO commonDAO;
	private File upFile;
	private String upFileContentType;
	private String upFileFileName;
	private static final int FILE_SIZE = 16 * 1024;


	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getMemberid() {
		return memberid;
	}
	public void setMemberid(String memberid) {
		this.memberid = memberid;
	}
	public String getGoodsid() {
		return goodsid;
	}
	public void setGoodsid(String goodsid) {
		this.goodsid = goodsid;
	}
	public String getSuc() {
		return suc;
	}
	public void setSuc(String suc) {
		this.suc = suc;
	}
	public String getNo() {
		return no;
	}
	public void setNo(String no) {
		this.no = no;
	}
	public int getIndex() {
		return index;
	}
	public void setIndex(int index) {
		this.index = index;
	}
	public CommonDAO getCommonDAO() {
		return commonDAO;
	}
	public void setCommonDAO(CommonDAO commonDAO) {
		this.commonDAO = commonDAO;
	}
	public File getUpFile() {
		return upFile;
	}
	public void setUpFile(File upFile) {
		this.upFile = upFile;
	}
	public String getUpFileContentType() {
		return upFileContentType;
	}
	public void setUpFileContentType(String upFileContentType) {
		this.upFileContentType = upFileContentType;
	}
	public String getUpFileFileName() {
		return upFileFileName;
	}
	public void setUpFileFileName(String upFileFileName) {
		this.upFileFileName = upFileFileName;
	}
	// 收藏
	public void collectAdd() {
		Map session= ServletActionContext.getContext().getSession();
		Member member = (Member)session.get("member");
		System.out.println("member==="+member);
		HttpServletResponse response=ServletActionContext.getResponse();
 		response.setContentType("text/html");
 		PrintWriter out;
		try {
		out = response.getWriter();
 		int responseContext;
 		if(member!=null){
 			ArrayList<Collect> list = (ArrayList<Collect>)commonDAO.findByHql("from Collect where memberid='"+member.getId()+"' and goodsid='"+goodsid+"'");
 			if(list.size()==0){
 			Collect collect = new Collect();
 			collect.setGoodsid(goodsid);
 			collect.setMemberid(member.getId().toString());
 			commonDAO.save(collect);
 			responseContext=0;	
 			}else{
 			responseContext=2;	
 			}
 			}else{
 			responseContext=1;	
 			}
 		out.println(responseContext);
 		out.flush();
 		out.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	
	//移出收藏
	public void collectDel() {
		Map session= ServletActionContext.getContext().getSession();
		HttpServletResponse response=ServletActionContext.getResponse();
 		response.setContentType("text/html");
 		PrintWriter out;
		try {
			System.out.println("id==="+id);
		out = response.getWriter();
 		int responseContext;
 		Collect collect = (Collect)commonDAO.findById(id, "Collect");
 		commonDAO.delete(collect);
 		responseContext=0;
 		out.println(responseContext);
 		out.flush();
 		out.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
