package com.action;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts2.ServletActionContext;

import com.dao.CommonDAO;
import com.model.Chat;
import com.model.Dddetail;
import com.model.Goods;
import com.model.Imgadv;
import com.model.Member;
import com.model.News;
import com.model.Pingjia;
import com.model.Protype;
import com.model.Sysuser;
import com.opensymphony.xwork2.ActionSupport;
import com.util.Info;
import com.util.MD5;
import com.util.Pagination;

/**
 * ���Թ���
 * @author Administrator
 *
 */
public class ChatAction extends ActionSupport
{
	private Integer id;
	private String memberid;
	private String content;
	private String hfcontent;
	private String savetime;
	private String hfsavetime;
	String suc;
	String error;
	String no;
	private int index=1;
	private CommonDAO commonDAO;
	
	
	
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getMemberid() {
		return memberid;
	}

	public void setMemberid(String memberid) {
		this.memberid = memberid;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public String getHfcontent() {
		return hfcontent;
	}

	public void setHfcontent(String hfcontent) {
		this.hfcontent = hfcontent;
	}

	public String getSavetime() {
		return savetime;
	}

	public void setSavetime(String savetime) {
		this.savetime = savetime;
	}

	public String getHfsavetime() {
		return hfsavetime;
	}

	public void setHfsavetime(String hfsavetime) {
		this.hfsavetime = hfsavetime;
	}

	public String getSuc() {
		return suc;
	}

	public void setSuc(String suc) {
		this.suc = suc;
	}

	public String getError() {
		return error;
	}

	public void setError(String error) {
		this.error = error;
	}

	public String getNo() {
		return no;
	}

	public void setNo(String no) {
		this.no = no;
	}

	public int getIndex() {
		return index;
	}

	public void setIndex(int index) {
		this.index = index;
	}

	public CommonDAO getCommonDAO() {
		return commonDAO;
	}

	public void setCommonDAO(CommonDAO commonDAO) {
		this.commonDAO = commonDAO;
	}

	//��������
	public String chatAdd(){
		HttpServletRequest request = ServletActionContext.getRequest();
		Member member = (Member)request.getSession().getAttribute("member");
		if(member!=null){
		Chat chat = new Chat();
		chat.setContent(content);
		chat.setMemberid(member.getId().toString());
		chat.setSavetime(Info.getDateStr());
		chat.setHfcontent("");
		chat.setHfsavetime("");
		commonDAO.save(chat);
		}else{
			no="���ȵ�¼";
		}
		return "success";
	}
	
	
	//���Իظ�
	public String chatHf(){
		Chat chat = (Chat)commonDAO.findById(id, "Chat");
		chat.setHfcontent(hfcontent);
		chat.setHfsavetime(Info.getDateStr());
		commonDAO.update(chat);
		Map session= ServletActionContext.getContext().getSession();
		Sysuser s = (Sysuser)session.get("admin");
		commonDAO.writeLog(s.getId(), "�ظ�����");
		return "success";
	}
	
}
