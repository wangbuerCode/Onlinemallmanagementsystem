package com.action;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts2.ServletActionContext;

import com.dao.CommonDAO;
import com.model.Chat;
import com.model.Goods;
import com.model.Imgadv;
import com.model.Member;
import com.model.Sysuser;
import com.opensymphony.xwork2.ActionSupport;
import com.util.Info;
import com.util.MD5;
import com.util.Pagination;

/**
 * 会员信息功能
 * @author Administrator
 *
 */
public class GoodsAction extends ActionSupport
{
	private Integer id;
	private String goodsno;
	private String goodname;
	private String fid;
	private String sid;
	private String goodpp;
	private String price;
	private String tprice;
	private String istj;
	private String content;
	private String filename1;
	private String filename2;
	private String filename3;
	String suc;
	String no;
	private int index=1;
	private CommonDAO commonDAO;
	private File[] upFile;
	private String upFileContentType;
	private String[] upFileFileName;
	private static final int FILE_SIZE=16*1024;
	
	
	
	
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getGoodsno() {
		return goodsno;
	}

	public void setGoodsno(String goodsno) {
		this.goodsno = goodsno;
	}

	public String getGoodname() {
		return goodname;
	}

	public void setGoodname(String goodname) {
		this.goodname = goodname;
	}

	public String getFid() {
		return fid;
	}

	public void setFid(String fid) {
		this.fid = fid;
	}

	public String getSid() {
		return sid;
	}

	public void setSid(String sid) {
		this.sid = sid;
	}

	public String getGoodpp() {
		return goodpp;
	}

	public void setGoodpp(String goodpp) {
		this.goodpp = goodpp;
	}

	public String getPrice() {
		return price;
	}

	public void setPrice(String price) {
		this.price = price;
	}

	public String getTprice() {
		return tprice;
	}

	public void setTprice(String tprice) {
		this.tprice = tprice;
	}

	public String getIstj() {
		return istj;
	}

	public void setIstj(String istj) {
		this.istj = istj;
	}

	public String getSuc() {
		return suc;
	}

	public void setSuc(String suc) {
		this.suc = suc;
	}

	public String getNo() {
		return no;
	}

	public void setNo(String no) {
		this.no = no;
	}

	public int getIndex() {
		return index;
	}

	public void setIndex(int index) {
		this.index = index;
	}

	public CommonDAO getCommonDAO() {
		return commonDAO;
	}

	public void setCommonDAO(CommonDAO commonDAO) {
		this.commonDAO = commonDAO;
	}

	public File[] getUpFile() {
		return upFile;
	}

	public void setUpFile(File[] upFile) {
		this.upFile = upFile;
	}

	public String getUpFileContentType() {
		return upFileContentType;
	}

	public void setUpFileContentType(String upFileContentType) {
		this.upFileContentType = upFileContentType;
	}


	public String[] getUpFileFileName() {
		return upFileFileName;
	}

	public void setUpFileFileName(String[] upFileFileName) {
		this.upFileFileName = upFileFileName;
	}
	

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	
	public String getFilename1() {
		return filename1;
	}

	public void setFilename1(String filename1) {
		this.filename1 = filename1;
	}

	public String getFilename2() {
		return filename2;
	}

	public void setFilename2(String filename2) {
		this.filename2 = filename2;
	}

	public String getFilename3() {
		return filename3;
	}

	public void setFilename3(String filename3) {
		this.filename3 = filename3;
	}

	//文件上传
	public void upLoadFile(File source,File target){
		  InputStream in=null;
		  OutputStream out=null;
		  try{
		   in=new BufferedInputStream(new FileInputStream(source),FILE_SIZE);
		   out=new BufferedOutputStream(new FileOutputStream(target),FILE_SIZE);
		   byte[] image=new byte[FILE_SIZE];
		   while(in.read(image)>0){
		    out.write(image);
		   }
		  }catch(IOException ex){
		   ex.printStackTrace();
		  }finally{
		   try{
		    in.close();
		    out.close();
		   }catch(IOException ex){
		    
		   }
		  }
		 }
	
	//新增商品
	public String goodsAdd() {
		Goods goods = new Goods();
		goodsno=Info.getAutoId();
		goods.setGoodno(goodsno);
		goods.setGoodname(goodname);
		goods.setFid(fid);
		goods.setSid(sid);
		goods.setGoodpp(goodpp);
		goods.setPrice(price);
		goods.setContent(content);
		goods.setTprice("");
		goods.setIstj("no");
		goods.setDelstatus("0");
		goods.setSavetime(Info.getDateStr());
		goods.setFilename1(filename1);
		goods.setFilename2(filename2);
		goods.setFilename3(filename3);
		
		commonDAO.save(goods);
		Map session= ServletActionContext.getContext().getSession();
		Sysuser s = (Sysuser)session.get("admin");
		commonDAO.writeLog(s.getId(), "新增商品");
		suc="";
		return "success";
	}
	
	//修改商品
	public String goodsEdit() {
		Goods goods =(Goods)commonDAO.findById(id, "Goods");
		goods.setGoodname(goodname);
		goods.setFid(fid);
		goods.setSid(sid);
		goods.setPrice(price);
		goods.setContent(content);
		goods.setFilename1(filename1);
		commonDAO.update(goods);
		Map session= ServletActionContext.getContext().getSession();
		Sysuser s = (Sysuser)session.get("admin");
		commonDAO.writeLog(s.getId(), "修改"+goods.getGoodno()+"商品");
		suc="";
		return "success";
	}
	
	public String goodstjSet(){
		Goods goods = (Goods)commonDAO.findById(id, "Goods");
		goods.setTprice(tprice);
		commonDAO.update(goods);
		Map s = new HashMap();
		return "success";
	}
	
	//推荐商品
	public String goodstuijian(){
		HttpServletRequest request = ServletActionContext.getRequest();
		request.getSession().setAttribute("price", content);
		return "success";
	}


}
