package com.action;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts2.ServletActionContext;

import com.dao.CommonDAO;
import com.model.Dddetail;
import com.model.Goods;
import com.model.Imgadv;
import com.model.Member;
import com.model.News;
import com.model.Pingjia;
import com.model.Protype;
import com.model.Sysuser;
import com.opensymphony.xwork2.ActionSupport;
import com.util.Info;
import com.util.MD5;
import com.util.Pagination;

/**
 * 商品评价功能
 * @author Administrator
 *
 */
public class PingjiaAction extends ActionSupport
{
	private Integer id;
	private String goodsid;
	private String memberid;
	private String content;
	private String savetime;
	String suc;
	String error;
	String no;
	private int index=1;
	private CommonDAO commonDAO;
	
	
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getGoodsid() {
		return goodsid;
	}
	public void setGoodsid(String goodsid) {
		this.goodsid = goodsid;
	}
	public String getMemberid() {
		return memberid;
	}
	public void setMemberid(String memberid) {
		this.memberid = memberid;
	}
	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}
	public String getSavetime() {
		return savetime;
	}
	public void setSavetime(String savetime) {
		this.savetime = savetime;
	}
	public String getSuc() {
		return suc;
	}
	public void setSuc(String suc) {
		this.suc = suc;
	}
	public String getNo() {
		return no;
	}
	public void setNo(String no) {
		this.no = no;
	}
	public int getIndex() {
		return index;
	}
	public void setIndex(int index) {
		this.index = index;
	}
	public CommonDAO getCommonDAO() {
		return commonDAO;
	}
	public void setCommonDAO(CommonDAO commonDAO) {
		this.commonDAO = commonDAO;
	}
	
	public String getError() {
		return error;
	}
	public void setError(String error) {
		this.error = error;
	}
	//新增评价
	public String pingjiaAdd(){
		HttpServletRequest request = ServletActionContext.getRequest();
		Member member = (Member)request.getSession().getAttribute("member");
		if(member!=null){
	    ArrayList<Dddetail> list = (ArrayList<Dddetail>)commonDAO.findByHql("from Dddetail where goodsid='"+goodsid+"' and memberid='"+member.getId()+"'");
	    if(list.size()!=0){
		Pingjia pingjia = new Pingjia();
		pingjia.setContent(content);
		pingjia.setMemberid(member.getId().toString());
		pingjia.setSavetime(Info.getDateStr());
		pingjia.setGoodsid(goodsid);
		commonDAO.save(pingjia);
		goodsid=goodsid;
		suc="评价成功";
	    }else{
	    error="没有购买此商品不能做出评价";	
	    }
		}else{
			no="请先登录";
		}
		return "success";
	}
	
	//删除评论
	public String pingjiaDel(){
		Pingjia pingjia = (Pingjia)commonDAO.findById(id, "Pingjia");
		commonDAO.delete(pingjia);
		Map session= ServletActionContext.getContext().getSession();
		Sysuser s = (Sysuser)session.get("admin");
		commonDAO.writeLog(s.getId(), "删除评论");
		return "success";
	}
}
