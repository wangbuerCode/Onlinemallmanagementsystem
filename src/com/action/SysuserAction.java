package com.action;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts2.ServletActionContext;

import com.dao.CommonDAO;
import com.model.Sysuser;
import com.opensymphony.xwork2.ActionSupport;
import com.util.Info;
import com.util.MD5;
import com.util.Pagination;

/**
 * 系统用户功能
 * @author Administrator
 *
 */
public class SysuserAction extends ActionSupport
{
	private int id;
	private String uname;
	private String upass;
	private String newupass;
	private String tname;
	private String sex;
	private String tel;
	private String email;
	private String addr;
	private String delstatus;
	private String savetime;
	private String utype;
	private String filename;
	private String logindate;
	String suc;
	String error;
	private int index=1;
	private CommonDAO commonDAO;
	
	
	
	
	
	public String getLogindate() {
		return logindate;
	}
	public void setLogindate(String logindate) {
		this.logindate = logindate;
	}
	public String getFilename() {
		return filename;
	}
	public void setFilename(String filename) {
		this.filename = filename;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getUname() {
		return uname;
	}
	public void setUname(String uname) {
		this.uname = uname;
	}
	public String getUpass() {
		return upass;
	}
	public void setUpass(String upass) {
		this.upass = upass;
	}
	public String getTname() {
		return tname;
	}
	public void setTname(String tname) {
		this.tname = tname;
	}
	public String getSex() {
		return sex;
	}
	public void setSex(String sex) {
		this.sex = sex;
	}
	public String getTel() {
		return tel;
	}
	public void setTel(String tel) {
		this.tel = tel;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getAddr() {
		return addr;
	}
	public void setAddr(String addr) {
		this.addr = addr;
	}
	public String getDelstatus() {
		return delstatus;
	}
	public void setDelstatus(String delstatus) {
		this.delstatus = delstatus;
	}
	public String getSavetime() {
		return savetime;
	}
	public void setSavetime(String savetime) {
		this.savetime = savetime;
	}
	public String getUtype() {
		return utype;
	}
	public void setUtype(String utype) {
		this.utype = utype;
	}
	public String getSuc() {
		return suc;
	}
	public void setSuc(String suc) {
		this.suc = suc;
	}
	public int getIndex() {
		return index;
	}
	public void setIndex(int index) {
		this.index = index;
	}
	public CommonDAO getCommonDAO() {
		return commonDAO;
	}
	public void setCommonDAO(CommonDAO commonDAO) {
		this.commonDAO = commonDAO;
	}
	
	public String getError() {
		return error;
	}
	public void setError(String error) {
		this.error = error;
	}
	
	
	public String getNewupass() {
		return newupass;
	}
	public void setNewupass(String newupass) {
		this.newupass = newupass;
	}
	//管理员登陆
	public String adminLogin(){
		Map session= ServletActionContext.getContext().getSession();
		String hql="from Sysuser where uname='"+uname+"' and upass='"+upass+"' and delstatus='0' ";
		List adminList=commonDAO.findByHql(hql);
		if(adminList.size()!=0){
			Sysuser sysuser=(Sysuser)adminList.get(0);
			sysuser.setLogindate(Info.getDateStr());
			commonDAO.update(sysuser);
			 session.put("admin", sysuser);
			 commonDAO.writeLog(sysuser.getId(), "登陆");
			 suc="";
			 return "success";
		}else{
			error="用户名或密码错误";
			return "error";
		}
	}
	//后台用户修改密码
	public String adminPwdEdit(){
		Sysuser s = (Sysuser)commonDAO.findById(id, "Sysuser");
		if(!s.getUpass().equals(upass)){
			error="旧密码不正确";
			return "erro";
		}else{
			s.setUpass(newupass);
			commonDAO.update(s);
			commonDAO.writeLog(s.getId(), "修改密码");
			suc="密码修改成功";
			return "success";
		}
		
	}
	//新增系统用户前检查用户名唯一性 jqery
	 public void adminnamecheck() throws IOException
	 {
	    	 ArrayList<Sysuser> list = (ArrayList<Sysuser>)commonDAO.findByHql("from Sysuser where uname='"+uname+"' and delstatus='0' ");
	    	 int responseContext;
	    	 if(list.size()==0){
	    		 responseContext = 0;
	    	 }else{
	    		 responseContext=1;
	    	 }
	 		HttpServletResponse response=ServletActionContext.getResponse();
	 		response.setContentType("text/html");
	 		PrintWriter out = response.getWriter();
	 		out.println(responseContext);
	 		out.flush();
	 		out.close();
	 }
	
	//新增系统用户
	public String useradd(){
		Sysuser s = new Sysuser();
		s.setUname(uname);
		s.setUpass(upass);
		s.setTname(tname);
		s.setSex(sex);
		s.setTel(tel);
		s.setEmail(email);
		s.setAddr(addr);
		s.setSavetime(Info.getDateStr());
		s.setDelstatus("0");
		s.setUtype("普通管理员");
		s.setFilename(filename);
		commonDAO.save(s);
		Map session= ServletActionContext.getContext().getSession();
		Sysuser ss = (Sysuser)session.get("admin");
		commonDAO.writeLog(ss.getId(), "新增管理员");
		suc="新增系统用户成功";
		return "success";
	}
	//编辑系统用户
	public String useredit(){
		Sysuser s = (Sysuser)commonDAO.findById(id, "Sysuser");
		s.setUpass(upass);
		s.setTname(tname);
		s.setSex(sex);
		s.setTel(tel);
		s.setEmail(email);
		s.setAddr(addr);
		s.setFilename(filename);
		commonDAO.update(s);
		Map session= ServletActionContext.getContext().getSession();
		Sysuser ss = (Sysuser)session.get("admin");
		commonDAO.writeLog(ss.getId(), "新增管理员");
		suc="编辑系统用户成功";
		return "success";
	}
	
	
	//会员退出
	public String memberExit(){
		Map session= ServletActionContext.getContext().getSession();
		session.remove("member");
		
		return "success";
	}
	
	//管理员退出
	public String adminExit(){
		Map session= ServletActionContext.getContext().getSession();
		Sysuser s = (Sysuser)session.get("admin");
		commonDAO.writeLog(s.getId(), "退出登录");
		session.remove("admin");
		
		return "success";
	}
	
	//编辑个人信息
	public String updateGrinfo(){
		Sysuser s = (Sysuser)commonDAO.findById(id, "Sysuser");
		s.setTname(tname);
		s.setSex(sex);
		s.setTel(tel);
		s.setEmail(email);
		s.setAddr(addr);
		s.setFilename(filename);
		commonDAO.update(s);
		commonDAO.writeLog(s.getId(), "编辑个人信息");
		suc="操作成功";
		return "success";
	}
	
	


	 
}
