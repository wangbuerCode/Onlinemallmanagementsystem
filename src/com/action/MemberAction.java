package com.action;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts2.ServletActionContext;

import com.dao.CommonDAO;
import com.joyce.mail.test.MailTest;
import com.model.Member;
import com.model.Sysuser;
import com.opensymphony.xwork2.ActionSupport;
import com.util.Info;
import com.util.MD5;
import com.util.Pagination;

/**
 * 会员信息功能
 * @author Administrator
 *
 */
public class MemberAction extends ActionSupport
{
	private Integer id;
	private String filename;
	private String username;
	private String userpassword;
	private String realname;
	private String sex;
	private String brithtime;
	private String tel;
	private String addr;
	private String email;
	private String delstatus;
	private String savetime;
	private String oldupass;
	private String newupass;
	private Integer loginerronum;
	private String lockstatus;
	private String isgjhy;
	private String sheng;
	private String shi;
	String suc;
	String no;
	private int index=1;
	private CommonDAO commonDAO;
	
	
	
	public String getSheng() {
		return sheng;
	}
	public void setSheng(String sheng) {
		this.sheng = sheng;
	}
	public String getShi() {
		return shi;
	}
	public void setShi(String shi) {
		this.shi = shi;
	}
	public String getIsgjhy() {
		return isgjhy;
	}
	public void setIsgjhy(String isgjhy) {
		this.isgjhy = isgjhy;
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getUserpassword() {
		return userpassword;
	}
	public void setUserpassword(String userpassword) {
		this.userpassword = userpassword;
	}
	public String getRealname() {
		return realname;
	}
	public void setRealname(String realname) {
		this.realname = realname;
	}
	public String getSex() {
		return sex;
	}
	public void setSex(String sex) {
		this.sex = sex;
	}
	public String getTel() {
		return tel;
	}
	public void setTel(String tel) {
		this.tel = tel;
	}
	public String getAddr() {
		return addr;
	}
	public void setAddr(String addr) {
		this.addr = addr;
	}
	public String getDelstatus() {
		return delstatus;
	}
	public void setDelstatus(String delstatus) {
		this.delstatus = delstatus;
	}
	public String getSavetime() {
		return savetime;
	}
	public void setSavetime(String savetime) {
		this.savetime = savetime;
	}
	public String getSuc() {
		return suc;
	}
	public void setSuc(String suc) {
		this.suc = suc;
	}
	public String getNo() {
		return no;
	}
	public void setNo(String no) {
		this.no = no;
	}
	public int getIndex() {
		return index;
	}
	public void setIndex(int index) {
		this.index = index;
	}
	public CommonDAO getCommonDAO() {
		return commonDAO;
	}
	public void setCommonDAO(CommonDAO commonDAO) {
		this.commonDAO = commonDAO;
	}
	
	public String getBrithtime() {
		return brithtime;
	}
	public void setBrithtime(String brithtime) {
		this.brithtime = brithtime;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	
	public String getOldupass() {
		return oldupass;
	}
	public void setOldupass(String oldupass) {
		this.oldupass = oldupass;
	}
	public String getNewupass() {
		return newupass;
	}
	public void setNewupass(String newupass) {
		this.newupass = newupass;
	}
	
	public String getFilename() {
		return filename;
	}
	public void setFilename(String filename) {
		this.filename = filename;
	}
	
	
	
	public Integer getLoginerronum() {
		return loginerronum;
	}
	public void setLoginerronum(Integer loginerronum) {
		this.loginerronum = loginerronum;
	}
	public String getLockstatus() {
		return lockstatus;
	}
	public void setLockstatus(String lockstatus) {
		this.lockstatus = lockstatus;
	}
	//会员登陆
	public String memberLogin(){
		Map session= ServletActionContext.getContext().getSession();
		String hql="from Member where username='"+username+"' and userpassword='"+userpassword+"' and delstatus='0' and lockstatus='n'";
		List adminList=commonDAO.findByHql(hql);
		

		if(adminList.size()!=0){
			Member member=(Member)adminList.get(0);
			member.setLoginerronum(0);
			commonDAO.update(member);
			 session.put("member", member);
		}else{
			hql = "from Member where username='"+username+"' and lockstatus='n'";
			List list=commonDAO.findByHql(hql);
			if(list.size()>0){
				Member mb = (Member)list.get(0);
				mb.setLoginerronum(mb.getLoginerronum()+1);
				if(mb.getLoginerronum()>=3){
					mb.setLockstatus("y");
				}
				commonDAO.update(mb);
			}
			no="用户名密码错误";
		}
		return "success";
	}
	//新增系统用户前检查用户名唯一性 jqery
	 public void usernamecheck() throws IOException
	 {
	    	 ArrayList<Member> list = (ArrayList<Member>)commonDAO.findByHql("from Member where username='"+username+"' and delstatus='0' ");
	    	 int responseContext;
	    	 System.out.println(list.size());
	    	 if(list.size()==0){
	    		 responseContext = 0;
	    	 }else{
	    		 responseContext=1;
	    	 }
	 		HttpServletResponse response=ServletActionContext.getResponse();
	 		response.setContentType("text/html");
	 		PrintWriter out = response.getWriter();
	 		out.println(responseContext);
	 		out.flush();
	 		out.close();
	 }
	
	//新用户注册
	public String regMember(){
		Member mb = new Member();
		mb.setUsername(username);
		mb.setFilename(filename);
		mb.setUserpassword(userpassword);
		mb.setRealname(realname);
		mb.setSex(sex);
		mb.setEmail(email);
		mb.setTel(tel);
		mb.setBrithtime(brithtime);
		mb.setAddr(addr);
		mb.setDelstatus("0");
		mb.setSavetime(Info.getDateStr());
		mb.setLoginerronum(0);
		mb.setLockstatus("n");
		mb.setIsgjhy("n");
		mb.setSheng(sheng);
		mb.setShi(shi);
		commonDAO.save(mb);
		suc="注册成功";
		return "success";
	}
	
	
	//编辑个人信息
	public String memberEdit(){
		Member mb = (Member)commonDAO.findById(id, "Member");
		mb.setRealname(realname);
		mb.setSex(sex);
		mb.setFilename(filename);
		mb.setEmail(email);
		mb.setTel(tel);
		mb.setBrithtime(brithtime);
		mb.setAddr(addr);
		mb.setSheng(sheng);
		mb.setShi(shi);
		commonDAO.update(mb);
		suc="编辑成功";
		return "success";
	}
	
	//会员退出
	public String memberExit(){
		Map session= ServletActionContext.getContext().getSession();
		session.remove("member");
		return "success";
	}
	
	//密码修改
	
	public String upassEdit(){
		Member member = (Member)commonDAO.findById(id, "Member");
		if(!member.getUserpassword().equals(oldupass)){
			no="原密码输入有误";
		}else{
			suc="修改成功";
			member.setUserpassword(newupass);
			commonDAO.update(member);
		}
		return "success";
	}
	//找回密码
	public String forgetpwd(){
		String hql = "from Member where username='"+username+"' and email='"+email+"' and delstatus='0' ";
		List<Member> list=(List<Member>)commonDAO.findByHql(hql);
		if(list.size()==1){
			MailTest.sendmial("你的密码是:"+list.get(0).getUserpassword()+",请立即前往重设密码",email);
		}
		suc="密码已发至邮箱，请注意查收";
		return "success";
	}
	
}
