package com.action;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts2.ServletActionContext;

import com.dao.CommonDAO;
import com.google.gson.Gson;
import com.model.Car;
import com.model.Goods;
import com.model.Imgadv;
import com.model.Member;
import com.model.Protype;
import com.model.Sysuser;
import com.opensymphony.xwork2.ActionSupport;
import com.util.Info;
import com.util.MD5;
import com.util.Pagination;

/**
 * 购物车功能
 * @author Administrator
 *
 */
public class CarAction extends ActionSupport
{
	private Integer id;
	private String goodsid;
	private int num;
	private String memberid;
	private String carid;
	private String jjtype;
	String suc;
	String no;
	private int index=1;
	private CommonDAO commonDAO;
	private File upFile;
	private String upFileContentType;
	private String upFileFileName;
	private static final int FILE_SIZE=16*1024;

	
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getGoodsid() {
		return goodsid;
	}
	public void setGoodsid(String goodsid) {
		this.goodsid = goodsid;
	}
	
	public int getNum() {
		return num;
	}
	public void setNum(int num) {
		this.num = num;
	}
	public String getMemberid() {
		return memberid;
	}
	public void setMemberid(String memberid) {
		this.memberid = memberid;
	}
	public String getSuc() {
		return suc;
	}
	public void setSuc(String suc) {
		this.suc = suc;
	}
	public String getNo() {
		return no;
	}
	public void setNo(String no) {
		this.no = no;
	}
	public int getIndex() {
		return index;
	}
	public void setIndex(int index) {
		this.index = index;
	}
	public CommonDAO getCommonDAO() {
		return commonDAO;
	}
	public void setCommonDAO(CommonDAO commonDAO) {
		this.commonDAO = commonDAO;
	}
	public File getUpFile() {
		return upFile;
	}
	public void setUpFile(File upFile) {
		this.upFile = upFile;
	}
	public String getUpFileContentType() {
		return upFileContentType;
	}
	public void setUpFileContentType(String upFileContentType) {
		this.upFileContentType = upFileContentType;
	}
	public String getUpFileFileName() {
		return upFileFileName;
	}
	public void setUpFileFileName(String upFileFileName) {
		this.upFileFileName = upFileFileName;
	}
	
	public String getCarid() {
		return carid;
	}
	public void setCarid(String carid) {
		this.carid = carid;
	}
	
	public String getJjtype() {
		return jjtype;
	}
	public void setJjtype(String jjtype) {
		this.jjtype = jjtype;
	}
	//添加到购物车
	public void carAdd(){
		Map session= ServletActionContext.getContext().getSession();
		Member member = (Member)session.get("member"); 
		HttpServletResponse response=ServletActionContext.getResponse();
 		response.setContentType("text/html");
 		PrintWriter out;
		try {
			out = response.getWriter();
		
 		int responseContext;
		if(member!=null){
			//检查该人的购物车是否有该物品
			ArrayList<Car>  cklist = (ArrayList<Car>)commonDAO.findByHql(" from Car where memberid='"+member.getId()+"' and goodsid='"+goodsid+"'");
			if(cklist.size()>0){
				Car car = cklist.get(0);
				car.setNum(car.getNum()+num);
				commonDAO.update(car);
			}else{
				Car car = new Car();
				car.setGoodsid(goodsid);
				car.setMemberid(member.getId().toString());
				car.setType("in");
				car.setNum(num);
				commonDAO.save(car);
			}
			responseContext = 0;
		}else{
			responseContext = 1;
		}
 		out.println(responseContext);
 		out.flush();
 		out.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	//改变数量
	public void changeNum(){
		Map session= ServletActionContext.getContext().getSession();
		HttpServletResponse response=ServletActionContext.getResponse();
 		response.setContentType("text/html");
 		PrintWriter out;
		try {
		out = response.getWriter();
 		int responseContext;
 		System.out.println("jjtype="+jjtype);
 		System.out.println("carid="+carid);
 		System.out.println("num="+num);
 		Car car = (Car)commonDAO.findById(carid, "Car");
 		System.out.println(jjtype.equals("1"));
 		if(jjtype.equals("1")&&car.getNum()-1>0){
 			car.setNum(car.getNum()-1);
 			commonDAO.update(car);
 			responseContext=1;
 		}else if(jjtype.equals("2")){
 			car.setNum(car.getNum()+1);
 			commonDAO.update(car);
 			responseContext=1;
 		}else if(jjtype.equals("3")){
 			car.setNum(num);
 			commonDAO.update(car);
 			responseContext=1;
 		}else{
 			responseContext=0;
 		}
 		out.println(responseContext);
 		out.flush();
 		out.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	//删除购物车商品
	public void delProduct(){
		Map session= ServletActionContext.getContext().getSession();
		HttpServletResponse response=ServletActionContext.getResponse();
 		response.setContentType("text/html");
 		PrintWriter out;
		try {
		out = response.getWriter();
 		int responseContext;
 		Car car = (Car)commonDAO.findById(carid, "Car");
 		commonDAO.delete(car);
 		responseContext=1;
 		out.println(responseContext);
 		out.flush();
 		out.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	
	//更新购物车金额
	public void updateTotal(){
		Map session= ServletActionContext.getContext().getSession();
		HttpServletResponse response=ServletActionContext.getResponse();
		HttpServletRequest request = ServletActionContext.getRequest();
 		response.setContentType("text/html");
 		PrintWriter out;
		try {
		out = response.getWriter();
 		String responseContext ="";
 		Member member = (Member)request.getSession().getAttribute("member");
 		String totalstr = "0.0";
 		double total = 0;
 		int carnum = 0;
 		if(member!=null){
 			ArrayList<Car> list = (ArrayList<Car>)commonDAO.findByHql("from Car where memberid='"+member.getId()+"'");
 			for(Car car :list){
 				Goods goods = (Goods)commonDAO.findById(car.getGoodsid(), "Goods");
 				if(goods.getTprice().equals("")){
 					total += car.getNum()*Double.parseDouble(goods.getPrice());	
 				}else{
 					total += car.getNum()*Double.parseDouble(goods.getTprice());	
 				}
 			}
 			totalstr = String.format("%.1f", total);
 			carnum = list.size();
 		}
 		responseContext=totalstr+","+carnum;
 		out.println(responseContext);
 		out.flush();
 		out.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
}
