package com.action;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts2.ServletActionContext;

import com.dao.CommonDAO;
import com.google.gson.Gson;
import com.model.Car;
import com.model.Dddetail;
import com.model.Ddinfo;
import com.model.Goods;
import com.model.Imgadv;
import com.model.Jyfeeset;
import com.model.Kcrecord;
import com.model.Member;
import com.model.Protype;
import com.model.Sysuser;
import com.opensymphony.xwork2.ActionSupport;
import com.util.Info;
import com.util.ListSortUtil;
import com.util.MD5;
import com.util.Pagination;

/**
 * 订单功能
 * @author Administrator
 *
 */
public class DdinfoAction extends ActionSupport
{
	private Integer id;
	private String ddno;
	private String memberid;
	private String ddtotal;
	private String fkstatus;
	private String shstatus;
	private String savetime;
	private String lxfs;
	private String remark;
	private String ids;
	String suc;
	String no;
	private int index=1;
	private CommonDAO commonDAO;
	private File upFile;
	private String upFileContentType;
	private String upFileFileName;
	private static final int FILE_SIZE=16*1024;

	private String sdate;
	private String edate;
	
	public String getSdate() {
		return sdate;
	}

	public void setSdate(String sdate) {
		this.sdate = sdate;
	}

	public String getEdate() {
		return edate;
	}

	public void setEdate(String edate) {
		this.edate = edate;
	}

	public String getIds() {
		return ids;
	}

	public void setIds(String ids) {
		this.ids = ids;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getDdno() {
		return ddno;
	}

	public void setDdno(String ddno) {
		this.ddno = ddno;
	}

	public String getMemberid() {
		return memberid;
	}

	public void setMemberid(String memberid) {
		this.memberid = memberid;
	}

	public String getDdtotal() {
		return ddtotal;
	}

	public void setDdtotal(String ddtotal) {
		this.ddtotal = ddtotal;
	}


	public String getFkstatus() {
		return fkstatus;
	}

	public void setFkstatus(String fkstatus) {
		this.fkstatus = fkstatus;
	}

	public String getShstatus() {
		return shstatus;
	}

	public void setShstatus(String shstatus) {
		this.shstatus = shstatus;
	}

	public String getSavetime() {
		return savetime;
	}

	public void setSavetime(String savetime) {
		this.savetime = savetime;
	}

	public String getSuc() {
		return suc;
	}

	public void setSuc(String suc) {
		this.suc = suc;
	}

	public String getNo() {
		return no;
	}

	public void setNo(String no) {
		this.no = no;
	}

	public int getIndex() {
		return index;
	}

	public void setIndex(int index) {
		this.index = index;
	}

	public CommonDAO getCommonDAO() {
		return commonDAO;
	}

	public void setCommonDAO(CommonDAO commonDAO) {
		this.commonDAO = commonDAO;
	}

	public File getUpFile() {
		return upFile;
	}

	public void setUpFile(File upFile) {
		this.upFile = upFile;
	}

	public String getUpFileContentType() {
		return upFileContentType;
	}

	public void setUpFileContentType(String upFileContentType) {
		this.upFileContentType = upFileContentType;
	}

	public String getUpFileFileName() {
		return upFileFileName;
	}

	public void setUpFileFileName(String upFileFileName) {
		this.upFileFileName = upFileFileName;
	}
	
	

	
	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public static int getFileSize() {
		return FILE_SIZE;
	}

	public String getLxfs() {
		return lxfs;
	}

	public void setLxfs(String lxfs) {
		this.lxfs = lxfs;
	}

	public String orderAdd() {
		Ddinfo ddinfo = new Ddinfo();
		Dddetail dddetail = new Dddetail();
		String ddno = Info.getAutoId();
		ddinfo.setDdno(ddno);
		ddinfo.setMemberid(memberid);
		Jyfeeset jy = (Jyfeeset)commonDAO.findById(1, "Jyfeeset");
		if(remark.equals("y")){
			ddtotal = String.valueOf(Double.valueOf(ddtotal)+Double.valueOf(jy.getFee()));
		}
		ddinfo.setDdtotal(ddtotal);
		
		ddinfo.setFkstatus("待付款");
		ddinfo.setShstatus("");
		ddinfo.setWlinfo("");
		ddinfo.setFhstatus("");
		ddinfo.setLxfs(lxfs);
		ddinfo.setSavetime(Info.getDateStr());
		
		ddinfo.setRemark(remark==null?"N":remark);
		
		commonDAO.save(ddinfo);
		ArrayList<Car> carlist = (ArrayList<Car>)commonDAO.findByHql("from Car where memberid="+memberid);
		
		String[] a = ids.split(",");
		for(int i=0;i<a.length;i++){
			Car car = (Car)commonDAO.findById(a[i], "Car");
			dddetail.setDdno(ddno);
			dddetail.setMemberid(memberid);
			dddetail.setGoodsid(car.getGoodsid());
			dddetail.setNum(car.getNum().toString());
			commonDAO.save(dddetail);
			commonDAO.delete(car.getId(), "Car");
		}
	
		
		return "success";
		
	}
	
	//订单支付
	public String ddFukuan() {
		Ddinfo ddinfo = (Ddinfo)commonDAO.findById(id, "Ddinfo");
		ddinfo.setFkstatus("已支付");
		ddinfo.setFhstatus("等待发货");
		ArrayList<Dddetail> deldddetaillist = (ArrayList<Dddetail>)commonDAO.findByHql("from Dddetail where ddno='"+ddinfo.getDdno()+"'");
		for(Dddetail deldddetail:deldddetaillist){
			deldddetail.setFkstatus("已支付");
			commonDAO.update(deldddetail);
		}
		
		commonDAO.update(ddinfo);
		return "success";
		
	}
	
	//发货
	public String faHuo() {
		Ddinfo ddinfo = (Ddinfo)commonDAO.findById(id, "Ddinfo");
		ArrayList<Dddetail> dddlist = (ArrayList<Dddetail>)commonDAO.findByHql("from Dddetail where ddno ='"+ddinfo.getDdno()+"'");
		
		boolean flag = true;
		for(Dddetail dddetail:dddlist){
			int inventoryrecord = commonDAO.getkc(Integer.parseInt(dddetail.getGoodsid()));
			if(inventoryrecord<Integer.parseInt(dddetail.getNum())){
				flag = false;
				break;
			}
		}
		if(flag==true){
			for(Dddetail ddd:dddlist){
				Kcrecord kcrecord = new Kcrecord();
				kcrecord.setHappennum(ddd.getNum());
				kcrecord.setType("out");
				kcrecord.setGid(ddd.getGoodsid());
				kcrecord.setSavetime(Info.getDateStr());
				commonDAO.save(kcrecord);
			}
			ddinfo.setFhstatus("已发货");
			commonDAO.update(ddinfo);
			Map session= ServletActionContext.getContext().getSession();
			Sysuser s = (Sysuser)session.get("admin");
			commonDAO.writeLog(s.getId(), ddinfo.getDdno()+"发货");
			suc="发货成功";
			return "success";
		}else{
			no="发货失败,库存不足";
			return "success";
		}
		
	}
	
	//确认收货
	public String ddShouhuo() {
		Ddinfo ddinfo = (Ddinfo)commonDAO.findById(id, "Ddinfo");
		ddinfo.setShstatus("已收货");
		commonDAO.update(ddinfo);
		return "success";
		
	}
	
	public String updatedd(){
		Ddinfo ddinfo = (Ddinfo)commonDAO.findById(id, "Ddinfo");
		ddinfo.setRemark(remark);
		commonDAO.update(ddinfo);
		return "success";
		
	}
	
	
	
	public void saletj() throws IOException
	 {
		String hql =  "FROM Goods a where a.delstatus=0  ";
		//,( ) 
		ArrayList<Goods> list = (ArrayList<Goods>)commonDAO.findByHql(hql);
		ArrayList<Goods> newlist =  new ArrayList<Goods>();
		for(Goods g:list){
			String h = "from Dddetail where goodsid='"+g.getId()+"' and fkstatus='已支付'";
			ArrayList<Dddetail> dlist = (ArrayList<Dddetail>)commonDAO.findByHql(h);
			int xl = 0;
			for(Dddetail dd:dlist){
				xl += Integer.valueOf(dd.getNum());
			}
			g.setNum(xl);
			newlist.add(g);
		}
		
		ListSortUtil<Goods> sortList = new ListSortUtil<Goods>();  
        sortList.sort(newlist, "num", "desc");  
		
	    	 String xdata = "[";
				String ydata = "[";
				for(int i=0;i<list.size();i++){
					Goods obj = newlist.get(i);
					xdata += "'"+obj.getGoodname()+"'";
					ydata += obj.getNum();
					if(i<list.size()-1){
						xdata+=",";
						ydata+=",";
					}
				}
				xdata += "]";
				ydata += "]";
				String rtn = xdata+"$"+ydata;
		 		HttpServletResponse response=ServletActionContext.getResponse();
		 		HttpServletRequest request = ServletActionContext.getRequest();
		 		request.setCharacterEncoding("utf-8");  
		 		response.setContentType("text/html;charset=utf-8");
		 		PrintWriter out = response.getWriter();
				Gson gson = new Gson();
				rtn = gson.toJson(rtn);
				System.out.println(rtn);
				out.write(rtn);

	 }
	
	
	public void moneytj() throws IOException{
		String sql = "select distinct date_format(savetime,'%Y-%m-%d') ,sum(ddtotal) as ddtotal from Ddinfo where fkstatus='已支付' ";
		if(!sdate.equals("")){
			sql += " and savetime>='"+sdate+"' ";
		}
		if(!edate.equals("")){
			sql += " and savetime<='"+edate+"' ";
		}
		sql += " group by date_format(savetime,'%Y-%m-%d')";
		ArrayList<Object[]> list = (ArrayList<Object[]>)commonDAO.findByHql(sql);
		//['周一','周二','周三','周四','周五','周六','周日']
		//[11, 11, 15, 13, 12, 13, 10],
		String xdata = "[";
		String ydata = "[";
		for(int i=0;i<list.size();i++){
			Object[] obj = list.get(i);
			xdata += "'"+obj[0]+"'";
			ydata += obj[1];
			if(i<list.size()-1){
				xdata+=",";
				ydata+=",";
			}
		}
		xdata += "]";
		ydata += "]";
		
		String rtn = xdata+"$"+ydata;
 		HttpServletResponse response=ServletActionContext.getResponse();
 		HttpServletRequest request = ServletActionContext.getRequest();
 		request.setCharacterEncoding("utf-8");  
 		response.setContentType("text/html;charset=utf-8");
 		PrintWriter out = response.getWriter();
		Gson gson = new Gson();
		rtn = gson.toJson(rtn);
		System.out.println(rtn);
		out.write(rtn);
	}
}
